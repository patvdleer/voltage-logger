#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include "RTClib.h"
#include <LiquidCrystal_I2C.h>
#include <Adafruit_ADS1X15.h>


/*-------- WIFI ----------*/
#include "secrets.h"

/*-------- SD CARD ----------*/
const int chipSelect = 10;
char filename[32];
File logFile;

/*-------- LCD ----------*/
LiquidCrystal_I2C lcd(0x27, 16, 2);

/*-------- ADS ----------*/
Adafruit_ADS1115 ads;

int16_t adc0;
uint16_t R1_0 = 10000; // in ohm
uint16_t R2_0 = 1000;  // in ohm

int16_t adc1;
uint16_t R1_1 = 10000; // in ohm
uint16_t R2_1 = 1000;  // in ohm

/*-------- NTP ----------*/
// Central European Time
// const int timeZoneOffset = 3600;

// Central European Time + summer time
const int timeZoneOffset = 7200;

#define compileTime DateTime(F(__DATE__), F(__TIME__))
#define ntpUpdateOnWifi false

RTC_DS1307 rtc;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", timeZoneOffset);

float readVoltage(uint8_t channel, uint16_t R1, uint16_t R2);
float readVoltage(uint8_t channel, uint16_t R1, uint16_t R2, uint8_t samples);
void displayVoltage();

char sbuf[32];

char * formatTime(DateTime dt) {
  sprintf(sbuf, "%i-%02d-%02dT%02d:%02d", dt.year(), dt.month(), dt.day(), dt.hour(), dt.minute());
  return sbuf;
}

char * formatTimeFull(DateTime dt) {
  sprintf(sbuf, "%i-%02d-%02dT%02d:%02d:%02d", dt.year(), dt.month(), dt.day(), dt.hour(), dt.minute(), dt.second());
  return sbuf;
}

bool network_available(const char * name) {
  int n = WiFi.scanNetworks();
  for (int i = 0; i < n; i++)
  {
    if(String(name) == WiFi.SSID(i)) {
      return true;
    }
  }
  return false;
}

void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("Booting");

  delay(100);
  Serial.print("Initializing LCD...");
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0,0);
	Serial.println();
  lcd.print("Booting");
  
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(10);
  Serial.println("Checking WiFi");
  if(network_available(ssid)){
    Serial.print("Found network, connecting...");
    WiFi.begin(ssid, password);
    for (short int i = 0; i < 30; i++) {
      if(WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      } else {
        Serial.println("connected!");
        Serial.print("IP address: ");
        Serial.print(WiFi.localIP());
        break;
      }
    }
    Serial.println("");
  } else {
    Serial.println("Found not network, turning off WiFi");
    WiFi.mode(WIFI_OFF);
  }

  if (!rtc.begin()) {
    lcd.setCursor(0,0);
    lcd.print("Couldn't find RTC");
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1);
  }

  if (! rtc.isrunning() || ntpUpdateOnWifi) {
    if (WiFi.status() == WL_CONNECTED) {
      timeClient.begin();
      delay(20);
      Serial.println("TimeClient updating");
      timeClient.update();
      Serial.println("RTC Updating");
      rtc.adjust(DateTime(timeClient.getEpochTime()));
    } else {        
      Serial.println("RTC setting to compileTime");
      rtc.adjust(compileTime);
    }
  }

  Serial.print("Initializing SD card...");
  if (!SD.begin(chipSelect)) {
    lcd.setCursor(0,0);
    lcd.print("Init SD failed");
    Serial.println("failed");
    Serial.flush();
    while (1);
  } else {
    Serial.println("success");
  }

  sprintf(filename, "%s.csv", formatTime(rtc.now()));

  Serial.print("Initializing ADS1115...");
  if (!ads.begin()) {
    lcd.setCursor(0,0);
    lcd.print("Init ADS1115 failed");
    Serial.println("Failed to initialize ADS.");
    while (1);
  } else {
    Serial.println("success");
  }

  ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 188uV = 0.1875mV
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 125uV

  lcd.clear();
}

float v1;
float v2;

void loop() {
  DateTime now = rtc.now();
  v1 = readVoltage(0, R1_0, R2_0);
  v2 = readVoltage(1, R1_1, R2_1);
  if(v1 <= 0.01) v1 = 0.0;
  if(v2 <= 0.01) v2 = 0.0;

  Serial.print("V: "); 
  Serial.println(v1, 2);
  Serial.print("V: "); 
  Serial.println(v2, 2);

  logFile = SD.open(filename, FILE_WRITE);
  logFile.print(formatTimeFull(now));
  logFile.print(",");
  logFile.print(v1, 2);
  logFile.print(",");
  logFile.println(v2, 2);
  logFile.close();

  displayVoltage();
}

void displayVoltage() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(v1);
  lcd.print("V");

  lcd.setCursor(0,1);
  lcd.print(v2);
  lcd.print("V");
}

float readVoltage(uint8_t channel, uint16_t R1, uint16_t R2) {
  return readVoltage(channel, R1, R2, 10);
}

float readVoltage(uint8_t channel, uint16_t R1, uint16_t R2, uint8_t samples) {
  float sum = 0;
  int i = 0;
  while (i < samples) {
    sum += ads.readADC_SingleEnded(channel);
    i++;
    delay(5);
  }
  return (sum / samples) * (6.144 / 32768) * ((R2 + R1)/R2);
}